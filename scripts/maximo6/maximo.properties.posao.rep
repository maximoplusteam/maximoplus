// ----------------------------------------------------------------------------
// MXServer.properties : Configuration file for MXServer
// ----------------------------------------------------------------------------

// Name to bind the MXServer server object to in the RMI registry
      mxe.name=MXUATServer

// Name of the machine and port hosting MXServer
	mxe.hostname=ghomaxapd2:8008
//	mxe.hostname=localhost:7003

// Port used by RMI for communication, if left at 0, RMI will use any available
// port on the system.  To use a specific port, set this parameter to an available 
// port number.
	  mxe.rmi.port=0
	  mxe.registry.port=9999

// The following setting should be used only in production environments
// to improve the performance of the MAXIMO application. The installation
// program used to setup MAXIMO application sets this parameter value
// to true to indicate that the MAXIMO application is configured for production
// use and the WebApplication layer should use local calls instead of 
// RMI calls using the RMI stubs to access MAXIMO business objects. 
// This setting should be set to false for development use or when developing 
// customizations to MAXIMO application. The default value is false, if not set.
		mxe.allowLocalObjects=true

// Enable the following setting if MAXIMO application is configured to use
// Application Server provided security. The default is to use MAXIMO security
// and the value is false. Setting this value to true implies that the
// MAXIMO security should be disabled. MAXIMO application would not work correctly
// if this setting is set to true and MAXIMO application is not configured to
// use Application Server security. The default value is false, if not set.
//		
mxe.useAppServerSecurity=false

// By default, the multi-language metadata cache will be loaded
// one object at a time upon request. Turn this flag to 1 if you want 
// the information if all objects be loaded together at once for one language.
//	mxe.MLCacheLazyLoad=1

// Indicate the User License ID 
	mxe.UserLicenseKey="LXN813907"
// --------------------------------------------------------------------------
// Database Related Properties
// --------------------------------------------------------------------------
// Database Schema Owner
   mxe.db.schemaowner=maximo
   
// Specification of JDBC Driver
// e.g. SqlServer   i-net opta 2000 driver
//      mxe.db.driver=com.inet.tds.TdsDriver
// e.g. DB2 driver:
//	mxe.db.driver=com.ibm.db2.jcc.DB2Driver
// for Oracle it is as follows...
//  oracle thin driver
	mxe.db.driver=oracle.jdbc.driver.OracleDriver

// JDBC "url" of database -- varies with the particular database you're
// connecting to... 
// e.g. SqlServer 6.5 i-net opta 2000 driver
//        mxe.db.url=jdbc:inetdae6:hostname:port?database=dbname&language=us_english&nowarnings=true
// e.g. SqlServer 7.0 or higher  i-net opta 2000 driver
//        mxe.db.url=jdbc:inetdae7a:hostname:port?database=dbname&language=us_english&nowarnings=true
// e.g. DB2:
//    mxe.db.url=jdbc:db2://localhost:50000/dbalias
// e.g Oracle thin   mxe.db.url=jdbc:oracle:thin:@<HOST>:<PORT>:<SID>
//  Oracle thin driver
//      mxe.db.url=jdbc:oracle:thin:@ghomaxdbu1-vip:1521:uatmax2
mxe.db.url=jdbc:oracle:thin:@(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = ghomaxdbu1)(PORT = 1522)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = orarep)))


// Database login name -- depends on driver, e.g. sometimes it's name=
//      If running under SQLServer, this user must have sysadmin role,
//	as defined via sp_addsrvrolemember.
// For DB2 this is an O/S user.
      mxe.db.user=maximo

// Database login password -- depends on driver, e.g. sometimes it's passwd=
// For DB2, this is the O/S password.
      mxe.db.password=max775rep

// Number of database connections needed to be kept open as soon as MXServer
// is started. Default value is 15.
      mxe.db.initialConnections=6
// Maximum number of free database connections available in the connection
// pool.  Default value is 30.
      mxe.db.maxFreeConnections=10
// Minimum number of free database connections available in the connection
// pool for more connections to be allocated. Default value is 10.
      mxe.db.minFreeConnections=4
// Number of new connections to be created when the minimum number of free connections
// are available in the connection pool. Default value is 5.      
      mxe.db.newConnectionCount=3

//    mxe.db.transaction_isolation : Must be one of...
//        TRANSACTION_NONE              TRANSACTION_READ_UNCOMMITTED
//	  TRANSACTION_READ_COMMITTED    TRANSACTION_REPEATABLE_READ
//    	  TRANSACTION_SERIALIZABLE
//        Note: Please see javadoc for java.sql.Connection 
//              Default value is TRANSACTION_SERIALIZABLE
      mxe.db.transaction_isolation=TRANSACTION_READ_COMMITTED
//
//    mxe.db.format.upper : Used to tell system the database's uppercase
//                          function. For example, on SqlServer this is UPPER
//                          on Access it is UCASE
      mxe.db.format.upper=UPPER

//    mxe.db.format.date  : Used to tell system the database's date function.
//                          A value of 'none' tells the system to pass thru the
//                          date value. Default is to use JDBC escape syntax.
//    mxe.db.format.time  : Similar to date above.
//    mxe.db.format.timestamp  : Similar to date above.
mxe.db.format.timestamp=todate
//
//    mxe.db.autocommit   : Used to set the autocommit mode used for the 
//                          Write connections. The default is false.
//                          Values are true or false, e.g.
    mxe.db.autocommit=false

// For SQLServer, mxe.db.systemdateformat should be set to getdate().
// For DB2, mxe.db.systemdateformat should be set to current timestamp.
// For ORACLE, mxe.db.systemdateformat should be set to sysdate.
// The default is the ORACLE setting.
// e.g. SqlServer:
 	mxe.db.systemdateformat=sysdate
// e.g. DB2:
//	mxe.db.systemdateformat=current timestamp


// For SQLServer, mxe.db.format.nullvalue should be set to ISNULL.
// For DB2, mxe.db.format.nullvalue should be set to COALESCE.
// For ORACLE, mxe.db.format.nullvalue should be set to NVL.
// The default is the ORACLE setting.
// mxe.db.format.nullvalue=NVL

// The following setting is used/needed only for SQL Server environment.
// The setting can be used to prefetch and close the cursor related to
// the Main application's result set and any lookup used by the applications.
// This setting should help in reducing the lock contention in SQLServer.
// Setting a value larger than 500 rows may result in some performance
// degradation, as the code will try to fetch all these rows into memory.
// optimal setting would be 200. If this setting is not enabled or set to 0,
// then the code would default to keeping the cursor open until the application
// determines that it's not needed any longer.
// mxe.db.sqlserverPrefetchRows=200


//Maximo Admin User.
	mxe.adminuserid=maxadmin


// The login ID and password of the user used at the time of 
// registering a new user.
// The user name specified here must have permissions to create users.
      mxe.system.reguser=7000110
      mxe.system.regpassword=maxgasco62

// --------------------------------------------------------------------------
// Administrator e-mail address - used as the default e-mail address
//                                in case the maximo user does not have 
//                                e-mail specified in the labor records.
        mxe.adminEmail=Maximo_Admin@gasco.ae

//---------------------------------------------------------------------------

// The name of the SMTP mail server
	mail.smtp.host=xxx

//  Charset used for email notifications.
	mxe.email.charset=UTF-8

// ---------------------------------------------------------------------------
// Workflow Related Properties
// ---------------------------------------------------------------------------

// The e-mail address of your system administrator.
	mxe.workflow.admin=Maximo_Admin@gasco.ae

// ----------------------------------------------------------------------------
// Reorder Related Properties
// ----------------------------------------------------------------------------

// The reorder preview timeout period (in minutes). It is recommended
// that this be comparable to the web server session timeout. 
// The default value is 30 minutes.

	mxe.reorder.previewtimeout=30
      
//----------------------------------------------------------------------------
// Security properties
//----------------------------------------------------------------------------
// mxe.security.provider=
// mxe.security.crypto.mode=
// mxe.security.crypto.padding=
// mxe.security.crypto.key=
// mxe.security.crypto.spec=
// mxe.security.cryptox.mode=
// mxe.security.cryptox.padding=
// mxe.security.cryptox.key=
// mxe.security.cryptox.spec=

//-----------------------------------------------------------------------------
// Cron Task Manager property.
//------------------------------------------------------------------------------
//Exclude the listed cron task instances from being loaded by this server.
//use ALL for not running any cron task.
mxe.crontask.donotrun=ALL
//Or specify the cron task instance by crontaskname.instancename
//mxe.crontask.donotrun=crontaskname.instancename

//-----------------------------------------------------------------------------
// Properties for debug. These parameters are test/debug purpose only.
//-----------------------------------------------------------------------------
// Displays the number of mbo objects created by the server.
 mxe.mbocount=YES

// MAXIMO logs the SQL statements that take longer than the specified time limit.
// The time is in milli seconds.To turn off this feature put 0 for value
   mxe.db.logSQLTimeLimit=10000

// MAXIMO logs information about Business objects that are created by fetching
// lot of rows from the database. The following parameter can be used to trace
// these business objects. When this setting is enabled, a stack trace is printed
// in the maximo log for every business object set that fetches beyond the set 
// limit of rows. The stack trace log is also repeated for every multiple of such
// fetches. Enable this parameter for test/debug purpose only.To turn off this feature
// put 0 for value 
	mxe.db.fetchResultLogLimit=20000

//-----------------------------------------------------------------------------
// Properties for actuate report server.
// All the properties are required. 
//-----------------------------------------------------------------------------

// The name of the report Server with encyclopedia to be accessed.
	        mxe.report.actuate.reportserver=ghomaxrpu1


// The URL of the active portal Server including port number and folder.
	        mxe.report.actuate.portalHost=http://ghomaxapu1:7001/acweb 


// The URL of the Report iServer including port number.
		mxe.report.actuate.iServer=http://ghomaxrpu1:8000 


// Actuate database connection string.	
	mxe.report.actuate.db.connectstring=UATMAX

// actuate encyclopedia root name
	mxe.report.actuate.rootEncycFolder=rpt

// actuate maximo server identifier
	mxe.report.actuate.rsseAlias=
//	mxe.report.actuate.rsseAlias=MXDevServer

// multiple maximo instances hitting same actuate server?
	mxe.report.actuate.multiServer=no
//	mxe.report.actuate.multiServer=yes


//-----------------------------------------------------------------------------
// Properties for eSignature
// All the properties are required. 
//-----------------------------------------------------------------------------

// flag whether the esignature login dialog should default the loginid
	mxe.esig.defaultuserid=true
